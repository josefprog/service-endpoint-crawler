#Service Endpoint Crawler

Utility app to retrieve webservice endpoints from application GIT repos (in BitBucket)

##Dependancies
- Use NodeJS 8.x, ES6 standard style
- Currently uses a static map of applications to git repos
- Uses BitBucket REST API to pull all Projects, then all repos for all Projects
- Uses Git command shell to shell out for a clone command
- Uses rimraf to execure a posix style rm -rf to purge a directory (clean-up prior to cloning)
- Unit tests in Mocha with Chai

##Utility Flow
Input - Requires App mappings to Git Repos from (currently) manuall applicaiton mapping
Output - Creates a CSV of Apps, Repos, and Endpoints for consumption in ServiceNow (ci details and mapping)
