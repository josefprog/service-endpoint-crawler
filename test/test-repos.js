'use strict'
/* eslint-env node, mocha */

let chai = require('chai')
let bitbucket = require('../lib/bitbucket')
let repo = require('../lib/repo')
let expect = chai.expect

describe('BitBucket Repo Population', function () {
  this.timeout(10000)
  it('should contain an array of Repos', function () {
    return bitbucket.getRepos()
    .then(function (repoList) {
      // Test Repo Collection
      expect(repoList).to.be.an('array')
      expect(repoList.length).to.be.greaterThan(0)
    })
  })
})

describe('Repo Tests', function () {
  this.timeout(10000)
  describe('Handling of Bad Data', function () {
    it('should return and empty array for a bad project', function () {
      return bitbucket.getRepos()
      .then(function (repoList) {
        // Test Repo Collection
        expect(repoList).to.be.an('array')
        expect(repoList.length).to.be.greaterThan(0)
      })
    })

    it('should return a null for a bad repo', function () {
      return bitbucket.getRepos()
      .then(function (repoList) {
        // Test Repo Collection by Name
        let testRepo1 = repo.getRepoByRepoName(repoList, 'Test1')
        expect(testRepo1).to.be.a('null')

        // Test Repo Collection by URL
        let testRepo2 = repo.getRepoByUrl(repoList, 'https://bitbucket.org/thingy/badrepothatdoesntexist')
        expect(testRepo2).to.be.a('null')
      })
    })
  })

  describe('Handling of Good Data', function () {
    it('should return a valid repo when given a good repo name', function () {
      return bitbucket.getRepos()
      .then(function (repoList) {
        // Test Repo Collection by Name
        let testRepo1 = repo.getRepoByRepoName(repoList, 'Approve.Me.Api')
        expect(testRepo1).to.be.an('object')
        expect(testRepo1.owner).to.be.equal('Progressive Leasing  Dev')

        // Test Repo Collection by URL
        let testRepo2 = repo.getRepoByUrl(repoList, 'https://bitbucket.org/progfin-ondemand/approve.me.api.git')
        expect(testRepo2).to.be.an('object')
        expect(testRepo2.owner).to.be.equal('Progressive Leasing  Dev')
      })
    })
  })
})
