'use strict'

// Get and Clone All Repos
let fs = require('fs')
let bitbucket = require('../../lib/bitbucket')
let git = require('../../lib/git')

let path = 'c:\\dev\\root2\\'

bitbucket.getRepos()
  .then(function (repoList) {
    repoList.forEach(function (repo) {
      // console.log(`Name: ${repo.name}, url: ${repo.clone_url}`)

      if (!fs.existsSync(path + repo.name)) {
        console.log(`Cloning repo ${repo.name} from ${repo.clone_url}`)
        let repoUrl = repo.clone_url.replace('toolworx@', 'toolworx:T00lW0rx@')
        git.cloneRepo(path + repo.name, repoUrl)
          .then(function () {
            console.log(`Cloned repo ${repo.name}`)
          })
          .catch(function (err) {
            console.log(err)
          })
      }
    })
  })
  .catch(function (err) {
    console.log(err)
  })
