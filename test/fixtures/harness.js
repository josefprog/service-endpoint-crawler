'use strict'

let fs = require('fs')
let git = require('simple-git')()

// git clone https://toolworx:T00lW0rx@bitbucket.org/progfin-ondemand/aarons-orders.git
let repoPath = 'https://toolworx:T00lW0rx@bitbucket.org/progfin-ondemand/aarons-orders.git'

git.clone(repoPath, 'test-repo', (err) => {
  if (err) {
    console.log(err)
    return
  }
  console.log('success')
})
