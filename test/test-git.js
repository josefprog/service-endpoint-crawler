'use strict'

/* eslint-env node, mocha */

let chai = require('chai')
let git = require('../lib/git')
let expect = chai.expect

describe('Git Cloning', function () {
  describe('Handling of Bad Data', function () {
    it('should return an error when given an empty path', function () {
      expect(function () { git.cloneRepo('', 'https://bitbucket.org/progfin-ondemand/bitbucket_integration.git') }).to.throw()
    })

    it('should return an error when given an empty repo', function () {
      expect(function () { git.cloneRepo('c:\\node\\temp', '') }).to.throw()
    })

    it('should return an error when given a bad path', function () {
      expect(function () { git.cloneRepo('Bad\\Path$$Name', 'https://bitbucket.org/progfin-ondemand/bitbucket_integration.git') }).to.throw()
    })

    // For dynamic App Data primarily
    it('should return an error if given an invalid repo (does not exist)', function () {
      return git.cloneRepo('c:\\node\\temp', 'http://bitbucket.org/bugus/BadRepo.git')
      .then(function (result) {
        // We should not get here
        throw new Error('Invalid Repo Url should have thrown an error and did not.')
      },
      function (err) {
        expect(err.indexOf('remote: Not Found')).to.be.greaterThan(-1)
      })
    })

    it('should return an error if given a malformed repo', function () {
      expect(function () { git.cloneRepo('c:\\node\\temp', 'htttttp://bitbucket.org^thing/bugus/BadRepo.git') }).to.throw()
    })
  })

  describe('Handling of Good Data', function () {
    this.timeout(10000)

    it('should return success after successfully cloning a public remote repo', function () {
      return git.cloneRepo('c:\\node\\temp\\git-pull-or-clone', 'https://github.com/feross/git-pull-or-clone.git')
      .then(function (result) {
        expect(result).to.equal('success')
      })
    })

    // BUG - Private repo security issues
    it('should return success after successfully cloning a private remote repo', function () {
      return git.cloneRepo('c:\\node\\temp\\bitbucket-integration', 'https://toolworx:T00lW0rx@bitbucket.org/progfin-ondemand/bitbucket_integration.git')
      .then(function (result) {
        expect(result).to.equal('success')
      })
    })
  })
})
