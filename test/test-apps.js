'use strict'
/* eslint-env node, mocha */

let chai = require('chai')
let apps = require('../lib/apps')
let expect = chai.expect

describe('Application Data', function () {
  describe('Handling of Bad Data', function () {
    it('should return an error when given a bad application', function () {
      expect(function () { apps.getApp('BadAppName') }).to.throw()
    })

    // For dynamic App Data primarily
    it('should return an error if the applications are missing', function () {
      expect(function () { apps.getApp('') }).to.throw()
    })
  })

  describe('Handling of Good Data', function () {
    it('should return the application array when queried', function () {
      let appArray = apps.getAllApps()
      expect(appArray.length).to.be.greaterThan(0)
    })
    it('should return a specified application when quieried directly', function () {
      let app = apps.getApp('ApprovalLimitService')
      expect(app.id).to.equal('de809a6b13cf3680ce8b7d576144b0dd')
      expect(app.name).to.equal('ApprovalLimitService')
    })
  })
})
