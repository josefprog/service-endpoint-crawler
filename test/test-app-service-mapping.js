'use strict'
/* eslint-env node, mocha */

let chai = require('chai')

let expect = chai.expect

describe('Application Mappings', function () {
  describe('Handling of Bad Data', function () {
    it('should return an error when given a bad application')
  })

  describe('Handling of Good Data', function () {
    it('should return a valid git repo object when given a good application')
  })
})
