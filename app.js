'use strict'

let fs = require('fs')
let path = require('path')
let parse = require('xml2js').parseString
var glob = require('glob-fs')({ gitignore: true })
let config = require('./config.json')
let appsLib = require('./lib/apps')
let bitbucket = require('./lib/bitbucket')
let repo = require('./lib/repo')
let git = require('./lib/git')

let resultMappings = []
let allApps = []

// STEP 1: Check App Repos (Should this be optional?)
// Get Repo Mappings
bitbucket.getRepos()
  .then(function (repoList) {
    allApps = appsLib.getAllApps()
    allApps.forEach(function (app) {
      // Connect Repos and Apps
      let appRepo = repo.getRepoByUrl(repoList, app.repo)
      if (appRepo === null) {
        // app repo is not valid
        console.log(`Invalid or Missing Repo entry for Application ${app.name}`)
      }
    })
  })
  .then(function () {
    // STEP 2: Clone Config Repo
    let configPath = path.join(__dirname, config.configRepo.name)
    git.cloneRepo(configPath, config.configRepo.repo)
    .then(function () {
      // Parse Config files for Endpoint mappings
      allApps.forEach(function (app) {
        let mapping = parseConfig(app, configPath)

        // add object to result array
        if (mapping !== undefined) {
          resultMappings.push(mapping)
        }
      })

      console.log('ForEach done...')

      writeDataToFile(resultMappings)
    })
  })

function parseConfig (app, configDir) {
  // Look for Web.Config
  let xmlConfigFile = path.join(configDir, app.name, 'PROD', 'Web.config')
  let results = null
  // Check file exists
  if (fs.existsSync(xmlConfigFile)) {
    // Config is Valid
    results = xmlParse(app, xmlConfigFile)
  }

  if (results !== null) return results

    // Look for other .Config files
  try {
    // let configPath = path.join('progressive-config', app.name, 'PROD', '*.config')
    let configPath = `progressive-config/${app.name}/PROD/*.config`
    let files = glob.readdirSync(configPath)

    if (files instanceof Array) {
      files.forEach(function (file) {
        // Check if the found file is part of the solution by name
        if (file.indexOf(app.name) > -1) {
          xmlConfigFile = path.join(__dirname, file)
          // Check file exists
          if (fs.existsSync(xmlConfigFile)) {
            // Config is Valid
            results = xmlParse(app, xmlConfigFile)
          }
        }
      })
    }
  } catch (err) {
    // Catch for missing files
  }

  if (results !== null) return results

  // Look for appsettings.json
  let jsonConfigFile = path.join(configDir, app.name, 'PROD', 'appsettings.json')

  // Check file exists
  if (fs.existsSync(jsonConfigFile)) {
    // Config is Valid
    results = jsonParse(app, jsonConfigFile)
  }

  if (results !== null) return results

  // Else
  console.log(`---> No Valid Config File found for ${app.name}`)
}

function jsonParse (app, configFile) {
  let results = {
    endpoints: [],
    connectionStrings: [],
    name: app.name,
    id: app.id,
    repo: app.repo
  }

  let jsonFile = fs.readFileSync(configFile, 'utf8')
  let result = JSON.parse(jsonFile.toString('utf8').replace(/^\uFEFF/, ''))

  if (result === undefined || result === null) {
    console.log(`Result is empty for ${configFile}`)
    return
  }

  // Looks for "Data Source=", "http"
  Object.entries(result).forEach(
      ([key, value]) => {
        if (typeof value === 'string') {
          if (value.indexOf('Data Source') > -1) {
            // Data Connections
            let item = {}
            item.name = key
            item.connectionString = value
            results.connectionStrings.push(item)
          } else if (value.toLowerCase().indexOf('http') > -1) {
          // Service Endpoints
            let item = {}
            item.name = key
            item.address = value
            results.endpoints.push(item)
          }
        }
      })

  return results
}

function xmlParse (app, configFile) {
  let results = {
    endpoints: [],
    connectionStrings: [],
    name: app.name,
    id: app.id,
    repo: app.repo
  }

  let xmlFile = fs.readFileSync(configFile)

  parse(xmlFile, function (err, result) {
    if (err) {
      console.log('Error: ' + err)
      return err
    }

    if (result === undefined || result === null) {
      console.log(`Result is empty for ${configFile}`)
      return
    }
    // Service Endpoints
    // Check is serviceModel Exists first
    if (result.configuration['system.serviceModel'] !== undefined &&
      result.configuration['system.serviceModel'][0].client !== undefined &&
      result.configuration['system.serviceModel'][0].client[0].endpoint !== undefined) {
      let endpoints = result.configuration['system.serviceModel'][0].client[0].endpoint

      // Pull All Configured EndPoints
      endpoints.forEach(function (indexItem) {
        let endpoint = indexItem['$']
        let item = {}
        item.name = endpoint.name
        item.address = endpoint.address
        results.endpoints.push(item)
      })
    }

    // Data Connections
    if (result.configuration.connectionStrings !== undefined &&
      result.configuration.connectionStrings[0].add !== undefined) {
      let connectionStrings = result.configuration.connectionStrings[0].add

      // Pull all configured ConnectionStrings
      connectionStrings.forEach(function (indexItem) {
        let conn = indexItem['$']
        let item = {}
        item.name = conn.name
        item.connectionString = conn.connectionString
        results.connectionStrings.push(item)
      })
    }

    // AppSettings
    if (result.configuration.appSettings !== undefined &&
      result.configuration.appSettings[0].add !== undefined) {
      let appSettings = result.configuration.appSettings[0].add

      // Check for other service endpoints in AppSettings
      appSettings.forEach(function (settingItem) {
        let setting = settingItem['$']
        if (setting.value.indexOf('http') > -1) {
          let item = {}
          item.name = setting.key
          item.address = setting.value
          results.endpoints.push(item)
        }
      })
    }
  })
  return results
}

function writeDataToFile (resultMappings) {
  console.log(`Writing out ${resultMappings.length} application entries to csv`)

  // Parse mapping object out to CSV
  let filename = config.outputFilename
  let content = 'id,name,repo,connectionName,connectionString,endpointName,endpoint\r\n'

  resultMappings.forEach(function (mapping) {
    // If No Conn && No EP - Write single line
    if (mapping.endpoints.length === 0 && mapping.connectionStrings.length === 0) {
      let line = `${mapping.id},${mapping.name},${mapping.repo},,,,\r\n`
      content += line
    } else {
      // Foreach Conn
      if (mapping.endpoints.length > 0) {
        mapping.endpoints.forEach(function (endpoint) {
          let line = `${mapping.id},${mapping.name},${mapping.repo},,,${endpoint.name},${endpoint.address}\r\n`
          content += line
        })
      }

      // Foreach EP
      if (mapping.connectionStrings.length > 0) {
        mapping.connectionStrings.forEach(function (connection) {
          let line = `${mapping.id},${mapping.name},${mapping.repo},${connection.name},${connection.connectionString},,\r\n`
          content += line
        })
      }
    }

    fs.writeFileSync(filename, content)
  })
}
