'use strict'

exports.getRepoByRepoName = function (repos, repoName) {
  // parse repos for repo that matches repo name
  for (var i = 0, len = repos.length; i < len; i++) {
    if (typeof repos[i] !== 'object') continue
    if (repos[i].name === repoName) return repos[i]
  }

  // Return null, as matching value was not found
  return null
}

exports.getRepoByUrl = function (repos, repoUrl) {
  let repoPart = repoUrl.replace('https://', '')
  for (var i = 0, len = repos.length; i < len; i++) {
    if (typeof repos[i] !== 'object') continue
    if (repos[i].clone_url.indexOf(repoPart) > -1) return repos[i]
  }

  // Return null, as matching value was not found
  return null
}
