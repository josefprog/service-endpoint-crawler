'use strict'

let appStub = require('./app-stub')
let _apps = []

// TODO: Replace stub here with dynamic endpoint call to ServiceNow
_apps = appStub

exports.getAllApps = function () {
  return _apps
}

exports.getApp = function (appName) {
  for (var i = 0, len = _apps.length; i < len; i++) {
    if (typeof _apps[i] !== 'object') continue
    if (_apps[i].name === appName) return _apps[i]
  }

  throw new Error('Could not find Application: ' + appName)
}
