'use strict'

let fs = require('fs')
let simpleGit = require('simple-git')()
let nuke = require('rimraf')

exports.cloneRepo = function (pathName, repoUrl) {
  // Test Path and Url and not blank or missing
  if (pathName === null || pathName === '') {
    throw new Error('Missing pathName parameter in cloneRepo')
  }
  if (repoUrl === null || repoUrl === '') {
    throw new Error('Missing repoUrl parameter in cloneRepo')
  }

  // check valid url
  if (!isURL(repoUrl)) {
    throw new Error('Invalid Repo URL - Fails format validation')
  }

  // Check path structure
  if (!isPath(pathName)) {
    throw new Error('Invalid Path - Fails format validation')
  }

  // clone the repo here
  return new Promise(function (resolve, reject) {
    // check if directory already exists
    if (fs.existsSync(pathName)) {
      // remove directory if needed
      nuke.sync(pathName, {}, function (err) {
        reject(err)
      })
    }

    simpleGit.clone(repoUrl, pathName, (err) => {
      if (err) {
        reject(err)
        return
      }
      resolve('success')
    })
  })
}

// Validation Functions
function isURL (str) {
  if (str.indexOf('@') > -1) return true

  let urlRegex = '^(?!mailto:)(?:(?:http|https|ftp)://)(?:\\S+(?::\\S*)?@)?(?:(?:(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}(?:\\.(?:[0-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))|(?:(?:[a-z\\u00a1-\\uffff0-9]+-?)*[a-z\\u00a1-\\uffff0-9]+)(?:\\.(?:[a-z\\u00a1-\\uffff0-9]+-?)*[a-z\\u00a1-\\uffff0-9]+)*(?:\\.(?:[a-z\\u00a1-\\uffff]{2,})))|localhost)(?::\\d{2,5})?(?:(/|\\?|#)[^\\s]*)?$'

  let url = new RegExp(urlRegex, 'i')

  return str.length < 2083 && url.test(str)
}

function isPath (contwinpath) {
  if ((contwinpath.charAt(0) !== '\\' || contwinpath.charAt(1) !== '\\') || (contwinpath.charAt(0) !== '/' || contwinpath.charAt(1) !== '/')) {
    if (!contwinpath.charAt(0).match(/^[a-zA-Z]/)) {
      return false
    }

    if (!contwinpath.charAt(1).match(/^[:]/) || !contwinpath.charAt(2).match(/^[\\]/)) {
      return false
    }
  }

  return true
}
