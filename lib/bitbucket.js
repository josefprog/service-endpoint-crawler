'use strict'

let request = require('request-promise-native')
let config = require('../config.json')

exports.getRepos = function () {
  return new Promise(function (resolve, reject) {
    // Return Repo Data - Refresh each call
    request(config.bitBucketProjectUrl)
    .then(function (projectList) {
      // Parse out project files
      parseRepos(projectList)
      .then(function (repoList) {
        resolve(repoList)
      })
    })
    .catch(function (err) {
      console.log('Error occured: ' + err)
    })
  })
}

function parseRepos (projectList) {
  // Return a Promise here
  return new Promise(function (resolve, reject) {
    let repoList = []
    let repoPromises = []

    // loop for each project slug
    JSON.parse(projectList).values.forEach(function (project) {
      // request repo api for project slug
      let repoUrl = config.bitBucketRepoUrl.replace('{{projectKey}}', project.key)

      repoPromises.push(request(repoUrl))
    }) // End forEach Loop

    // TODO: Need to Add Pagination to the BitBuck return set
    Promise.all(repoPromises)
        .then(function (repoResults) {
          // forEach resultset
          repoResults.forEach(function (repoString) {
            // add repos to repo list
            let repoSet = JSON.parse(repoString)
            if (repoSet.values.length !== 0) {
              repoSet.values.forEach(function (repo) {
                if (repo.name != null) {
                  let repoItem = {
                    name: repo.name,
                    description: repo.description,
                    owner: repo.owner.display_name,
                    clone_url: repo.links.clone[0].href,
                    html_url: repo.links.html.href
                  }

                  repoList.push(repoItem)
                }
              })
            }
          })
        })
        .then(function () {
          resolve(repoList)
        })
  })
}
